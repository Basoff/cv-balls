import cv2
import numpy as np
import random

bColors = ["R", "G", "Y", "B"]
seq = []

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, -4)

balls = []
answer = False

while cam.isOpened():
    ret, frame = cam.read()
    blurred = cv2.GaussianBlur(frame, (21, 21), 0)
    #blurred = frame

    HSV = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

    lower_red = np.array([0, 100, 100])
    upper_red = np.array([5, 255, 255])
    mask_r = cv2.inRange(HSV, lower_red, upper_red)
    mask_r = cv2.bitwise_and(frame, frame, mask = mask_r)
    mask_r = cv2.cvtColor(mask_r, cv2.COLOR_BGR2GRAY)
    mask_r = cv2.threshold(mask_r, 25, 255, cv2.THRESH_BINARY)[1]
    mask_r = cv2.erode(mask_r, None, iterations = 2)
    mask_r = cv2.dilate(mask_r, None, iterations = 2)
    cont_r, _ = cv2.findContours(mask_r, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(cont_r) > 0:
        c = max(cont_r, key = cv2.contourArea)
        (x, y), radius = cv2.minEnclosingCircle(c)
        if radius > 10:
            cv2.circle(frame, (int(x), int(y)), int(radius), (0, 0, 255), 2)
            balls.append(["R", int(x)])

    lower_yel = np.array([20, 100, 100])
    upper_yel = np.array([30, 255, 255])
    mask_y = cv2.inRange(HSV, lower_yel, upper_yel)
    mask_y = cv2.bitwise_and(frame, frame, mask = mask_y)
    mask_y = cv2.cvtColor(mask_y, cv2.COLOR_BGR2GRAY)
    mask_y = cv2.threshold(mask_y, 25, 255, cv2.THRESH_BINARY)[1]
    mask_y = cv2.erode(mask_y, None, iterations = 2)
    mask_y = cv2.dilate(mask_y, None, iterations = 2)
    cont_y, _ = cv2.findContours(mask_y, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(cont_y) > 0:
        c = max(cont_y, key = cv2.contourArea)
        (x, y), radius = cv2.minEnclosingCircle(c)
        if radius > 10:
            cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 255), 2)
            balls.append(["Y", int(x)])

    lower_g = np.array([45, 90, 70])
    upper_g = np.array([65, 255, 255])
    mask_g = cv2.inRange(HSV, lower_g, upper_g)
    mask_g = cv2.bitwise_and(frame, frame, mask = mask_g)
    mask_g = cv2.cvtColor(mask_g, cv2.COLOR_BGR2GRAY)
    mask_g = cv2.threshold(mask_g, 25, 255, cv2.THRESH_BINARY)[1]
    mask_g = cv2.erode(mask_g, None, iterations = 2)
    mask_g = cv2.dilate(mask_g, None, iterations = 2)
    cont_g, _ = cv2.findContours(mask_g, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(cont_g) > 0:
        c = max(cont_g, key = cv2.contourArea)
        (x, y), radius = cv2.minEnclosingCircle(c)
        if radius > 10:
            cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 0), 2)
            balls.append(["G", int(x)])

    lower_blu = np.array([90, 100, 100])
    upper_blu = np.array([100, 255, 255])
    mask_b = cv2.inRange(HSV, lower_blu, upper_blu)
    mask_b = cv2.bitwise_and(frame, frame, mask = mask_b)
    mask_b = cv2.cvtColor(mask_b, cv2.COLOR_BGR2GRAY)
    mask_b = cv2.threshold(mask_b, 25, 255, cv2.THRESH_BINARY)[1]
    mask_b = cv2.erode(mask_b, None, iterations = 2)
    mask_b = cv2.dilate(mask_b, None, iterations = 2)
    cont_b, _ = cv2.findContours(mask_b, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(cont_b) > 0:
        c = max(cont_b, key = cv2.contourArea)
        (x, y), radius = cv2.minEnclosingCircle(c)
        if radius > 10:
            cv2.circle(frame, (int(x), int(y)), int(radius), (255, 0, 0), 2)
            balls.append(["B", int(x)])

    order = []

    for i in range(len(balls)-1):
        MinId = 0
        for j in range(len(balls)):
            if balls[j][1] < balls[MinId][1]:
                MinId = j
        order.append(balls.pop(MinId))
    if order:
        order.append(balls.pop())
        
    B = [i[0] for i in order]    
    if (seq != []) and (B == seq):
        if answer:
            cv2.putText(frame, f"{B} : {seq}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 160, 0))
        else:
            cv2.putText(frame, f"{B}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 160, 0))
    else:
        if answer:
            cv2.putText(frame, f"{B} : {seq}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (160, 0, 0))
        else:
            cv2.putText(frame, f"{B}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (160, 0, 0))
    cv2.imshow("Camera", frame)
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    if key == ord(']'):
        answer = not answer
    if (seq != []) and (key == ord('r')):
        seq = random.sample(bColors, len(seq))
    if key == ord('1'):
        seq = random.sample(bColors, 1)
    if key == ord('2'):
        seq = random.sample(bColors, 2)
    if key == ord('3'):
        seq = random.sample(bColors, 3)
    if key == ord('4'):
        seq = random.sample(bColors, 4)

cam.release()
cv2.destroyAllWindows()